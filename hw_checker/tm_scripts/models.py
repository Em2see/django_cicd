from django.db import models


class Script(models.Model):
    date_added = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    major_version = models.IntegerField()
    minor_version = models.IntegerField()
    build_tag = models.CharField(max_length=64)
    active = models.BooleanField(default=True)
    changes = models.CharField(max_length=256)
    source = models.FileField()

    def __str__(self):
        return (f"TM_script ver. {self.major_version}"
                f".{self.minor_version}"
                f".{self.build_tag}")