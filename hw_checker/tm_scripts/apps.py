from django.apps import AppConfig


class TmScriptsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tm_scripts'
