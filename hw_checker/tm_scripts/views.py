from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic import View


class GetFileView(View):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['latest_articles'] = None
        return context


class GetAllVersions(ListView):
    pass


class AddNewVersion(CreateView):
    pass