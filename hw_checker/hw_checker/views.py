from django.views.generic.base import ContextMixin


class MenuContextMixin(ContextMixin):

    def get_context_data(self, **kwargs):
        context = super(MenuContextMixin, self).get_context_data(**kwargs)
        context['menu'] = [('index', 'Home')]
        context['auth_menu'] = [('students', 'Students'),
                                ('modules', 'Modules'),
                                ('tasks', 'Tasks'),
                                ('statistics', 'Statistics'),
                                ('dev_api', 'DEV_API')]
        return context


class BaseMixin(MenuContextMixin):
    """
    Mixin for all pages
    """