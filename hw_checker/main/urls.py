from django.contrib.auth.views import (LogoutView, PasswordResetView,
                                       PasswordResetDoneView, PasswordResetConfirmView,
                                       PasswordResetCompleteView, LoginView)
from django.urls import path
from .views import IndexView, RegisterUserView, ProfileUpdateView, StudentsView, ModulesView, TasksView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('register/', RegisterUserView.as_view(), name='register'),
    path('login/', LoginView.as_view(template_name='auth/login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('profile/', ProfileUpdateView.as_view(), name='profile'),
    path('students/', StudentsView.as_view(), name='students'),
    path('student/<int:pk>', IndexView.as_view(), name='student'),
    path('modules/', ModulesView.as_view(), name='modules'),
    path('module/<int:pk>', IndexView.as_view(), name='module'),
    path('tasks/', TasksView.as_view(), name='tasks'),
    path('statistics/', IndexView.as_view(), name='statistics'),
    # path('api/', )б
    path('dev_api/', IndexView.as_view(), name='dev_api'),
]
