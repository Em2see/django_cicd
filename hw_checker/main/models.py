from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from loguru import logger


MAX_CODE_LEN = 128 * 1024
MAX_HASH_LEN = 256


class Profile(models.Model):
    UNVERIFIED = 1
    VERIFIED = 2
    STATUS_VERIFIED = (
        (UNVERIFIED, "Не верифицирован"),
        (VERIFIED, "Верифицирован")
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telegram_id = models.CharField(max_length=17, blank=True, verbose_name='telegarm')
    is_verified = models.IntegerField(choices=STATUS_VERIFIED,
                                      default=UNVERIFIED,
                                      null=False,
                                      verbose_name='верификация')
    email = models.CharField(max_length=100, default='', verbose_name='email')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        logger.debug('create profile for user ', instance.pk)
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    logger.debug('update profile for user ', instance.pk)
    instance.profile.save()


# we need to think about line number
class Line(models.Model):
    hash = models.CharField(max_length=MAX_HASH_LEN)


# we need to think about token position
class Token(models.Model):
    token_type = models.CharField(max_length=MAX_HASH_LEN)
    value = models.CharField(max_length=MAX_HASH_LEN)


class Student(models.Model):
    name = models.CharField(max_length=50)
    link = models.CharField(max_length=50)
    similarity_percent = models.IntegerField()


class Template(models.Model):
    link = models.CharField(max_length=50)
    name = models.CharField(max_length=50)


class LearnModule(models.Model):
    name = models.CharField(max_length=50)
    link = models.CharField(max_length=50)
    template = models.ForeignKey(Template, related_name='forks', null=True, on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True, blank=True, verbose_name='дата создания')
    student = models.ForeignKey(Student, related_name='modules', null=True, on_delete=models.CASCADE)
    is_empty = models.BooleanField(default=False)
    percentage = models.IntegerField(default=0)


class Task(models.Model):
    code = models.TextField(max_length=MAX_CODE_LEN)
    hash = models.CharField(max_length=MAX_HASH_LEN)
    black_hash = models.CharField(max_length=MAX_HASH_LEN)
    clean_hash = models.CharField(max_length=MAX_HASH_LEN)
    lines = models.ManyToManyField(Line, related_name='tasks', through='LinesTask')
    tokens = models.ManyToManyField(Token, related_name='tasks', through='TokensTask')
    module = models.ForeignKey(LearnModule, related_name='tasks', null=True, on_delete=models.CASCADE)


class LinesTask(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    line = models.ForeignKey(Line, on_delete=models.CASCADE)
    line_number = models.IntegerField()


class TokensTask(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    token = models.ForeignKey(Token, on_delete=models.CASCADE)
    token_line = models.IntegerField()
    token_pos = models.IntegerField()



