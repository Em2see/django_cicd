from django.contrib import admin
from .models import Student, Token, Task, Line, LearnModule, Profile, Template


class StudentAdmin(admin.ModelAdmin):
    model = Student


class TokenAdmin(admin.ModelAdmin):
    model = Token


class DocumentAdmin(admin.ModelAdmin):
    model = Task


class LineAdmin(admin.ModelAdmin):
    model = Line


class LearnModuleAdmin(admin.ModelAdmin):
    model = LearnModule


class ProfileAdmin(admin.ModelAdmin):
    model = Profile


class TemplateAdmin(admin.ModelAdmin):
    model = Template


admin.site.register(Student, StudentAdmin)
admin.site.register(Token, TokenAdmin)
admin.site.register(Task, DocumentAdmin)
admin.site.register(Line, LineAdmin)
admin.site.register(LearnModule, LearnModuleAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Template, TemplateAdmin)