from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse, NoReverseMatch
from django.views.generic import TemplateView, CreateView, UpdateView, ListView
from .models import Profile, LearnModule, Student, Task
from loguru import logger
from hw_checker.views import BaseMixin


class IndexView(TemplateView, BaseMixin):
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['menu'] = [('index', 'Home')]
        context['auth_menu'] = []
        auth_menu = [('students', 'Students'),
                     ('modules', 'Modules'),
                     ('tasks', 'Tasks'),
                     ('statistics', 'Statistics'),
                     ('dev_api', 'dev_API')]
        for url_name, menu_name in auth_menu:
            try:
                reverse(url_name)
                context['auth_menu'].append((url_name, menu_name))
            except NoReverseMatch as ex:
                logger.error(ex)

        return context


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    # form_class = ProfileForm
    fields = ['telegram_id', 'email']
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'auth/update_user.html'

    # TODO modify first_name, last_name in User model
    def get_object(self):
        logger.info(self.request.user)
        obj = self.model.objects.get(user=self.request.user)
        logger.info(obj)
        return obj

    def get_success_url(self):
        return reverse('index')


class RegisterUserView(CreateView):
    """
    Представление для регистрации
    """
    form_class = UserCreationForm
    template_name = 'auth/register.html'
    success_url = '/'

    def form_valid(self, form):
        valid = super(RegisterUserView, self).form_valid(form)
        user = authenticate(
            request=self.request,
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1']
        )
        login(self.request, user)

        return valid


class StudentsView(BaseMixin, ListView):
    model = Student
    paginate_by = 20
    template_name = "main/students.html"
    context_object_name = 'students_list'
    DATE_FORMAT = '%Y-%b-%d'

    def get_queryset(self):
        all_modules = self.model.objects.all()

        return all_modules

    def get_context_data(self, **kwargs):
        context = super(StudentsView, self).get_context_data(**kwargs)
        return context


class ModulesView(BaseMixin, ListView):
    model = LearnModule
    paginate_by = 20
    template_name = "main/modules.html"
    context_object_name = 'modules_list'
    DATE_FORMAT = '%Y-%b-%d'

    def get_queryset(self):
        all_modules = self.model.objects.all()

        return all_modules

    def get_context_data(self, **kwargs):
        context = super(ModulesView, self).get_context_data(**kwargs)
        # model._meta.get_field(header).verbose_name.title()
        # logger.info(dir(self.model._meta))
        # logger.info(self.model._meta.fields)
        return context


class TasksView(BaseMixin, ListView):
    model = Task
    paginate_by = 20
    template_name = "main/tasks.html"
    context_object_name = 'tasks_list'
    DATE_FORMAT = '%Y-%b-%d'

    def get_queryset(self):
        all_modules = self.model.objects.all()

        return all_modules

    def get_context_data(self, **kwargs):
        context = super(TasksView, self).get_context_data(**kwargs)
        return context
