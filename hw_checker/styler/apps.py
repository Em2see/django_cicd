from django.apps import AppConfig


class StylerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'styler'
