#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import os
from flask import Flask
from flask_migrate import Migrate
from flask.cli import FlaskGroup
from flask.cli import AppGroup, pass_script_info
from .server import create_app, db

from .models import Line, Token, Document
from .models import doc_lines, doc_tokens

migrate = Migrate()

def manage_create_app():
    app = create_app()
    migrate.init_app(app, db)
    # other setup
    return app


@click.group(cls=FlaskGroup, create_app=manage_create_app)
@pass_script_info
def cli(info, **kwargs):
    """
       Management script for Color Report application.
    """
    pass
    # # print(os.environ)
    # if kwargs['main_db'] is not None:
    #     os.environ['MAIN_DB_URL'] = kwargs['main_db']
    # if kwargs['imgs_db'] is not None:
    #     os.environ['IMGS_DB_URL'] = kwargs['imgs_db']
    # # print(info)
    # # print(kwargs)
    # print(os.environ.get('MAIN_DB_URL'))