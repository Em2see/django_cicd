__author__ = 'Dmitry Petrov'
__email__ = 'e2m3x4@gmail.com'
__version__ = '0.1.0'

# from dotenv import load_dotenv, find_dotenv

# load_dotenv(find_dotenv())

from .cli import cli, 
from .cli import manage_create_app as create_app

__all__ = ['cli', 'create_app']