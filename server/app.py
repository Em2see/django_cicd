#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_debugtoolbar import DebugToolbarExtension
from flask_login import LoginManager
import os
import sys
import logging

db = SQLAlchemy()
# login_manager = LoginManager()

def create_app():
    app = Flask(__name__, static_folder=None, instance_relative_config=True)
    app.logger.setLevel(logging.INFO)

    if 'FLASK_CONFIG' in os.environ:
        app.config.from_envvar('FLASK_CONFIG')
    else:
        # print(os.environ)
        environment_configuration = os.environ.get('CONFIGURATION_SETUP')
        app.config.from_object(environment_configuration)

    # login_manager.init_app(app)
    # login_manager.login_view = 'login_bp.login'
    toolbar = DebugToolbarExtension(app)

    db.init_app(app)

    app_blueprints(app, None)

    return app


def app_blueprints(app, login_manager=None):
    with app.app_context():
        from .model import User
        # from .model_imgs import Image
        from .styler_bp import styler_bp
        # from .login import login_bp
        
        app.register_blueprint(styler_bp, url_prefix='/')
        # app.register_blueprint(login_bp)

        # @login_manager.user_loader
        # def load_user(id):
        #     return User.query.get(int(id))
