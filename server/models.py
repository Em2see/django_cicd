#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .app import db


MAX_CODE_LEN = 128 * 1024
MAX_HASH_LEN = 256


doc_lines = db.Table('doc_lines',
    db.Column('doc', db.Integer, db.ForeignKey('documents.id'), primary_key=True),
    db.Column('line', db.Integer, db.ForeignKey('lines.id'), primary_key=True),
    db.Column('line_number', db.Integer, primary_key=True)
)

doc_tokens = db.Table('doc_tokens',
    db.Column('doc', db.Integer, db.ForeignKey('documents.id'), primary_key=True),
    db.Column('token', db.Integer, db.ForeignKey('tokens.id'), primary_key=True),
    db.Column('position', db.Integer, primary_key=True)
)

class Line(db.Model):
    __tablename__ = 'lines'

    id = db.Column(db.Integer, primary_key=True)
    hash = db.Column(db.String(MAX_HASH_LEN))
    docs = db.relationship('Document', secondary=doc_lines, lazy=True, passive_deletes=True)


class Token(db.Model):
    __tablename__ = 'tokens'
    
    id = db.Column(db.Integer, primary_key=True)
    ttype = db.Column(db.String(MAX_HASH_LEN))
    value = db.Column(db.String(MAX_HASH_LEN))
    docs = db.relationship('Document', secondary=doc_tokens, lazy=True, passive_deletes=True)


class Document(db.Model):
    __tablename__ = 'documents'
    
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(MAX_CODE_LEN))
    date_added = db.Column(db.DateTime)
    hash = db.Column(db.String(MAX_HASH_LEN))
    black_hash = db.Column(db.String(MAX_HASH_LEN))
    clean_hash = db.Column(db.String(MAX_HASH_LEN))
    lines = db.relationship('Line', secondary=doc_lines, lazy='subquery', passive_deletes=True)
    tokens = db.relationship('Token', secondary=doc_tokens, lazy=True, passive_deletes=True)