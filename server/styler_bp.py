#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Blueprint, url_for, make_response, send_from_directory
from flask import jsonify, send_file, request, g, render_template, current_app
import json
import logging
import os
from io import TextIOWrapper
from pylint import epylint
from tempfile import NamedTemporaryFile
import re
from flask_login import current_user, login_required

styler_bp = Blueprint('styler_bp', __name__, template_folder='../templates', static_folder='../static_files', static_url_path='/static')
styler_bp.config = {}
styler_bp.logger = None
styler_bp.paths = {}

@styler_bp.record
def record_params(setup_state):
    app = setup_state.app
    styler_bp.config = {key:value for key,value in app.config.items()}
    styler_bp.logger = app.logger
    styler_bp.paths = styler_bp.config['PATHS']
    
@styler_bp.route('/', methods=['GET','POST'])
def index():
    return render_template('index.html', current_user=current_user)

def add_prfix(code):
    return '\n'.join([
        "#!/usr/bin/env python", 
        "# -*- coding: utf-8 -*-",
        code
    ])

@styler_bp.route('/style', methods=['POST'])
def styler():
    
    #if request.is_json:
    req = request.get_json()
    
    tmp_file = NamedTemporaryFile(dir='./', mode='w', encoding='utf-8', delete=False, suffix='.py')
    lines = req['code']
    for i, line in enumerate(lines):
        match = re.match('^( +)([^ ].+)$', line)
        if match is not None:
            spaces = match.groups()[0]
            lines[i] = spaces * 2 + match.groups()[1]
    file_source = '\n'.join(lines)
    tmp_file.write(file_source)
    tmp_file.close()

    filename = os.path.basename(tmp_file.name)

    command = filename + ' -d C0114,C0103'
    # print(command)
    (pylint_stdout, pylint_stderr) = epylint.py_run(command, return_std=True)
    report = []
    while True:
        line = pylint_stdout.readline()
        if not line:
            break
        #the real code does filtering here
        line = line.strip()
        if len(line) > 0:
            report.append(line)
        
    errors = []
    while True:
        line = pylint_stderr.readline()
        if not line:
            break
        #the real code does filtering here
        errors.append(line.rstrip())
    
    os.unlink(tmp_file.name)
    return jsonify({"response": 'ok', 'report': report, 'err': errors, 'code': file_source})

@styler_bp.route('/')
@styler_bp.route('/index')
def index():
    return '<h1>Welcome to SB styler page</h1>'

if __name__ == '__main__':
    app.debug = True
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
    app.logger.setLevel(logging.DEBUG)
    app.run()
